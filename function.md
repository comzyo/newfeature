# Function

## Default Parameter

> In __Python__ way:

``` python
def say(name='Lamfunction'):
    print("Hi, %s" % name)  # For Python3.x
    pint "Hi, %s\n" % name  # For Python2.x

# call
say()            # Result: Hi, Lamfunction
say("Kim")       # Result: Hi, Kim
say("Carry")     # Result: Hi, Carry
```

> `C++11/4` way:

``` c++
void say(std::string name="Lamfunction")
{
    std::cout << "Hi, " << name << std::endl;
}

// call
say();
say("Kim");
say("Carry");
```