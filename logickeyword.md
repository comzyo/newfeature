# Logic Keyword

## __`And`__

> *Like symbol `&&`, From Cpp11 has new keyword -> `and`*

__Example__:

``` c++
#include <iostream>

int main(void)
{
    double x(3.03);

    if ((x > 3.00) and (x < 3.05)) {
	std::cout << "This is and statement." << std::endl;
    }

    return 0;
}
```

## __`Or`__

> *Like symbol `||` , From Cpp11 has new keyword -> `or`*

__Example__:

``` c++
#include <iostream>

int main(void)
{
    double y(5.03);

    if ((y > 3.00) or (y < 3.05)) {
	std::cout << "This is or statement." << std::endl;
    }

    std::cout << "Nothing." << std::endl;

    return 0;
}
```

## __`Not`__

> *Like symbol `!`, From Cpp11 has new keyword -> `not`*

__Example__:

``` c++
#include <iostream>

int main(void)
{
    double z(7.05);

    if (not (z == 7)) {
            std::cout << "This is not statement" << std::endl;
    }
}
```