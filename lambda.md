# Lambda Expression

## Basic

> *Two way*

1. `[] () {}`
2. `[] ()->ReturnType {}`

__In C++14, Parameter can use type with* `auto` *keyword.__

## Example

> C++11

``` c++
#include <iostream>

int main(void)
{
    auto adder = [] (int a, int b)->int { return a + b; };
    auto add   = [] (int a, int b) { return a + b; };

    std::cout << "6+4=" << adder(6,4) << std::endl;
    std::cout << "2+3=" << add(2,3)   << std::endl;

    return 0;
}
```

> C++14

``` c++
#include <iostream>

int main(void)
{
    auto adder = [] (auto a, auto b) { return a + b; };

    std::cout << "6+4=" << adder(6,4) << std::endl;
    std::cout << "5.5+4.5=" << adder(5.5, 4.5) << std::endl;

    return 0;
}
```
